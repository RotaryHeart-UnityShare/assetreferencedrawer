# Current Version
0.5.3-preview

# What's implemented?
This class works the same way as the default Unity `AssetReferenceDrawer`, the only difference is that it has been modified to allow it to draw even if the `AssetReference` is not at the root of the selected inspector.
Also, a new type reference check has been included that allows to filter the dropdown elements based on the AssetReferenceT type used. Here's an example:

```sh
[System.Serializable]
public class MaterialAssetReference : AssetReferenceT<Material> { }
public MaterialAssetReference myAsset;
```
This will filter the dropdown data to only show materials. While you can still achieve this with the default drawer using the attribute `[AssetReferenceTypeRestriction(typeof(Material))]` it will fail to filter if you are using `AssetReferenceT`.

You can read more about the drawing issue <a href="https://forum.unity.com/threads/error-rendering-drawer-for-assetreference-property.551125/#post-3799636">here</a>

# How to use it?
Just copy the content of the <a href="https://gitlab.com/RotaryHeart-UnityShare/assetreferencedrawer/blob/master/AssetReferenceDrawer.cs">AssetReferenceDrawer</a> file and paste it into `Packages/Addressables System/Editor/GUI/AssetReferenceDrawer.cs`